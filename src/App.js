import Chat from 'bsa';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
      <ToastContainer autoClose={2000} position="top-right" theme="dark" />
    </>
  );
}

export default App;
