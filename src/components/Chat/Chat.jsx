import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { fetchMessages } from 'services/api/chatApi';
import { useLocalStorage } from 'hooks';
import {
  sortMessagesByDate,
  countTotalParticipants,
  getMessageQty,
  getLastMessageDate,
  getCurrentDate,
} from 'helpers';
import {
  MessageList,
  Header,
  MessageInput,
  Preloader,
  ErrorNotification,
} from 'components';
import STATUS from 'constants/status';

function Chat({ url }) {
  const [status, setStatus] = useState(STATUS.IDLE);
  const [messages, setMessages] = useLocalStorage('messages', []);
  // const [messages, setMessages] = useState([]);
  const [messageToEdit, setMessageToEdit] = useState(null);

  useEffect(() => {
    setStatus(STATUS.PENDING);

    fetchMessages(url)
      .then(messages => {
        const sortedByDateMessages = sortMessagesByDate(messages);
        setMessages(sortedByDateMessages);
        setStatus(STATUS.RESOLVED);
      })
      .catch(error => {
        console.log(error);
        setStatus(STATUS.REJECTED);
      });
  }, [setMessages, url]);

  const handleMessageSubmit = newMessage => {
    const hasId = newMessage.id;

    hasId
      ? setMessages(messages =>
          messages.map(message =>
            message.id === hasId
              ? { ...newMessage, editedAt: getCurrentDate() }
              : message,
          ),
        )
      : setMessages(messages => [
          ...messages,
          {
            id: uuidv4(),
            userId: 'personal',
            avatar: '',
            user: 'Me',
            text: newMessage.text,
            createdAt: getCurrentDate(),
            editedAt: '',
          },
        ]);

    setMessageToEdit(null);
  };

  const handleMessageDelete = id => {
    setMessages(messages => messages.filter(message => message.id !== id));
  };

  const handleMessageEdit = id => {
    setMessageToEdit(messages.find(message => message.id === id));
  };

  const handleMessageLike = id => {
    const newMessages = messages.map(message =>
      message.id === id ? { ...message, isLiked: !message.isLiked } : message,
    );

    setMessages(newMessages);
  };

  switch (status) {
    case STATUS.IDLE:
      return null;

    case STATUS.PENDING:
      return (
        <>
          <Preloader />
        </>
      );

    case STATUS.RESOLVED:
      return (
        <div className="chat-wrapper">
          <Header
            participantsQty={countTotalParticipants(messages)}
            messageQty={getMessageQty(messages)}
            lastMessageDate={getLastMessageDate(messages)}
          />
          {messages.length !== 0 ? (
            <MessageList
              messages={messages}
              onDelete={handleMessageDelete}
              onEdit={handleMessageEdit}
              onLike={handleMessageLike}
            />
          ) : (
            <span>No messages yet</span>
          )}
          <MessageInput
            message={messageToEdit}
            onSubmit={handleMessageSubmit}
          />
        </div>
      );

    case STATUS.REJECTED:
      return <ErrorNotification />;

    default:
      return;
  }
}

export default Chat;

Chat.propTypes = {
  url: PropTypes.string.isRequired,
};
