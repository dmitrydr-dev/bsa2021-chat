import './ErrorNotification.css';

export function ErrorNotification() {
  return (
    <div className="error-notification">
      <h2 className="error-notification-message">Error on Fetching Data</h2>
    </div>
  );
}
