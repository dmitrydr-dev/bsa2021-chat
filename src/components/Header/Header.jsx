import PropTypes from 'prop-types';
import { getLocalDate, getLocalTime } from 'helpers';
import './Header.css';

export function Header({ participantsQty, messageQty, lastMessageDate }) {
  return (
    <header className="header">
      <h1 className="header-title">Marty's Ozark Casino</h1>
      <div className="header-info">
        {participantsQty > 0 ? (
          <span className="header-users-count">
            participants: {participantsQty}
          </span>
        ) : (
          <span className="header-users-count">This chat is empty</span>
        )}
        {messageQty > 0 ? (
          <span className="header-messages-count">messages: {messageQty}</span>
        ) : (
          <span className="header-messages-count">No messages yet</span>
        )}
        {lastMessageDate ? (
          <span className="header-last-message-date">
            Last Message at: {getLocalDate(lastMessageDate)}{' '}
            {getLocalTime(lastMessageDate)}
          </span>
        ) : null}
      </div>
    </header>
  );
}

Header.propTypes = {
  participantsQty: PropTypes.number.isRequired,
  messageQty: PropTypes.number.isRequired,
  lastMessageDate: PropTypes.string.isRequired,
};
