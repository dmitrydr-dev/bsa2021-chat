import PropTypes from 'prop-types';
import { getLocalTime } from 'helpers';
import { ReactComponent as LikeSvg } from 'assets/images/star.svg';
import './Message.css';

export function Message({ avatar, text, user, createdAt, onLike }) {
  return (
    <div className="message">
      <div className="message-avatar-section">
        <div className="message-avatar-wrap">
          <img src={avatar} alt={user} className="message-user-avatar" />
        </div>
      </div>
      <div className="message-body">
        <div className="message-text-block">
          <p className="message-user-name">{user}</p>
          <p className="message-text">{text}</p>
        </div>
        <div className="message-stats">
          <label className="message-like-wrap">
            <input type="checkbox" className="message-like" onChange={onLike} />

            <LikeSvg className="message-like-icon" width="20" height="20" />
          </label>
          <span className="message-time">{getLocalTime(createdAt)}</span>
        </div>
      </div>
    </div>
  );
}

Message.propTypes = {
  avatar: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  onLike: PropTypes.func.isRequired,
};
