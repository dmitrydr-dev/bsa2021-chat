import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import './MessageInput.css';

export function MessageInput({ onSubmit, message }) {
  const [messageText, setMessageText] = useState('');
  const [currentMessage, setCurrentMessage] = useState(null);

  useEffect(() => {
    if (message) {
      setCurrentMessage(message);
      setMessageText(message.text);
    }
  }, [message]);

  const handleChange = e => {
    setMessageText(e.target.value);
    setCurrentMessage(message => ({
      ...message,
      text: messageText,
    }));
  };

  const handleSubmit = e => {
    e.preventDefault();

    if (messageText === '') {
      return toast.error('Enter the message!');
    }

    onSubmit(currentMessage);
    setMessageText('');
    setCurrentMessage('');
  };

  return (
    <form className="message-input" onSubmit={handleSubmit}>
      <textarea
        className="message-input-text"
        value={messageText}
        onChange={handleChange}
        placeholder="Type here"
      ></textarea>
      <button type="submit" className="message-input-button">
        Send
      </button>
    </form>
  );
}

MessageInput.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};
