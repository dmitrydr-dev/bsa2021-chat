import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getSortedMessagesByDays, getLastTwoDays } from 'helpers';
import { Message } from 'components';
import { OwnMessage } from 'components/OwnMessage/OwnMessage';
import './MessageList.css';

export function MessageList({ messages, onDelete, onEdit, onLike }) {
  const [sortedMessages, setSortedMessages] = useState([]);

  useEffect(() => {
    const result = getSortedMessagesByDays(messages);
    setSortedMessages(result);
  }, [messages]);

  const { normalizedToday, normalizedYesterday } = getLastTwoDays();

  return (
    <section className="messages-group">
      {sortedMessages.map(([key, array]) => (
        <div key={key}>
          <div className="messages-divider">
            {key === normalizedToday ? (
              <div className="messages-divider-tag">Today</div>
            ) : key === normalizedYesterday ? (
              <div className="messages-divider-tag">Yesterday</div>
            ) : (
              <div className="messages-divider-tag">{key}</div>
            )}
          </div>
          <ul className="message-list">
            {array.map(({ id, avatar, text, user, userId, createdAt }) =>
              userId !== 'personal' ? (
                <li key={id} className="message-item">
                  <Message
                    avatar={avatar}
                    text={text}
                    user={user}
                    createdAt={createdAt}
                    onLike={() => {
                      onLike(id);
                    }}
                  />
                </li>
              ) : (
                <li key={id} className="own-message-item">
                  <OwnMessage
                    text={text}
                    createdAt={createdAt}
                    onDelete={() => {
                      onDelete(id);
                    }}
                    onEdit={() => {
                      onEdit(id);
                    }}
                  />
                </li>
              ),
            )}
          </ul>
        </div>
      ))}
    </section>
  );
}

MessageList.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      avatar: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
    }),
  ).isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
