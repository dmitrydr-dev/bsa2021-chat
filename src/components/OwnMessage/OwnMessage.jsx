import PropTypes from 'prop-types';
import { getLocalTime } from 'helpers';
import { ReactComponent as LikeSvg } from 'assets/images/star.svg';
import './OwnMessage.css';
export function OwnMessage({ text, createdAt, onDelete, onEdit }) {
  return (
    <div className="own-message">
      <div className="own-message-body">
        <div className="own-message-text-block">
          <p className="message-text">{text}</p>
        </div>
      </div>
      <div className="own-message-stats">
        <span className="message-time">{getLocalTime(createdAt)}</span>
        <label className="message-like-wrap">
          <input type="checkbox" className="message-like" disabled />

          <LikeSvg className="message-like-icon" width="20" height="20" />
        </label>
        <button type="button" className="own-message-button" onClick={onEdit}>
          Edit
        </button>
        <button type="button" className="own-message-button" onClick={onDelete}>
          Delete
        </button>
      </div>
    </div>
  );
}

OwnMessage.propTypes = {
  createdAt: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
