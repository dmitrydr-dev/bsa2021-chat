import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import './Preloader.css';

export function Preloader() {
  return (
    <div className="loaderWrap">
      <Loader type="Bars" color="#00BFFF" height={175} width={175} />
    </div>
  );
}
