export function getLocalTime(dateString) {
  const date = new Date(dateString);
  const options = {
    hour: '2-digit',
    minute: '2-digit',
  };
  return date.toLocaleTimeString([], options);
}

export function getLocalDate(dateString) {
  const date = new Date(dateString);
  const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  };
  return date.toLocaleDateString([], options);
}

export function getCurrentDate() {
  const date = new Date();
  return date.toISOString();
}

export function getLastTwoDays() {
  const today = new Date();
  let yesterday = new Date();
  yesterday.setDate(today.getDate() - 1);

  return {
    normalizedToday: getLocalDate(today),
    normalizedYesterday: getLocalDate(yesterday),
  };
}
