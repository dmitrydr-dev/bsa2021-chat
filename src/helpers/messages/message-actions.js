import { getLocalDate } from 'helpers';

export function sortMessagesByDate(messageArr) {
  return messageArr.sort(
    (msgA, msgB) => new Date(msgA.createdAt) - new Date(msgB.createdAt),
  );
}

export function countTotalParticipants(messageArr) {
  const users = messageArr.reduce((acc, msg) => [...acc, msg.userId], []);

  return [...new Set(users)].length + 1;
}

export function getMessageQty(messageArr) {
  return messageArr.length;
}

export function getLastMessageDate(messageArr) {
  const sortedArr = sortMessagesByDate(messageArr);
  return sortedArr[messageArr.length - 1].createdAt;
}

export function getSortedMessagesByDays(messageArr) {
  // const newArr = [];
  const result = {};

  messageArr.forEach(message => {
    const date = getLocalDate(message.createdAt);

    if (!result[date]) {
      result[date] = [];
    }

    result[date].push(message);
  });

  return Object.entries(result);
}
