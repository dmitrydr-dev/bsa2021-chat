export async function fetchMessages(url, method = 'GET') {
  const options = { method };

  try {
    const response = await fetch(url, options);
    return await response.json();
  } catch (error) {
    console.warn('Ok Houston, we have a problem:', error.message);
  }
}
